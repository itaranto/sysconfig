# sysconfig

My Linux system configuration.

It contains the system-wide configuration for the software I use. This repository is meant to be
paired with: https://gitlab.com/itaranto/dotfiles

Here's information about the computers I use:

- Personal desktop computer "terra"

  * Operating System: Arch Linux
  * Architecture: x86-64
  * Hardware Vendor: Gigabyte Technology Co., Ltd.
  * Hardware Model: AB350-Gaming 3
  * CPU: AMD Ryzen 7 2700X Eight-Core Processor

- Work laptop computer "luna"

  * Operating System: Fedora Linux 38 (Workstation Edition)
  * Architecture: x86-64
  * Hardware Vendor: Lenovo
  * Hardware Model: ThinkPad L15 Gen 4
  * CPU: AMD Ryzen 7 PRO 7730U with Radeon Graphics

  _"luna" isn't the current computer's hostname anymore, it has a corporate-managed one now._
