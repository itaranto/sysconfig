# shellcheck shell=sh
# shellcheck disable=SC2086

cache_home=${HOME}/.cache
config_home=${HOME}/.config
data_home=${HOME}/.local/share
state_home=${HOME}/.local/state

###############################################################################
# XDG workarounds
###############################################################################
# Rust
export CARGO_HOME=${data_home}/cargo

# Go
export GOPATH=${data_home}/go

# Python
export PYTHONSTARTUP=${config_home}/pythonrc
export PYTHON_HISTORY=${state_home}/python_history

# Ruby
export GEM_HOME=${data_home}/gem
export GEM_SPEC_CACHE=${GEM_HOME}/specs

# NodeJS
export NPM_CONFIG_USERCONFIG=${config_home}/npm/npmrc
export NPM_PACKAGES=${HOME}/.local
export NODE_PATH=${NPM_PACKAGES}/lib/node_modules:${NODE_PATH}
export NODE_REPL_HISTORY=${state_home}/node_repl_history

# General
export ANDROID_HOME=${data_home}/android
export AWS_CONFIG_FILE=${config_home}/aws/config
export AWS_SHARED_CREDENTIALS_FILE=${config_home}/aws/credentials
export COOKIECUTTER_CONFIG=${config_home}/cookiecutterrc
export DATABRICKS_CONFIG_FILE=${config_home}/databrickscfg
export DOCKER_CONFIG=${config_home}/docker
export GNUPGHOME=${data_home}/gnupg
export GRADLE_USER_HOME=${data_home}/gradle
export GTK2_RC_FILES=${config_home}/gtk-2.0/gtkrc
export INPUTRC=${config_home}/inputrc
export IPYTHONDIR=${config_home}/ipython
export KDEHOME=${config_home}/kde
export LESSHISTFILE=${state_home}/lesshst
export LESSKEY=${config_home}/lesskey
export MINIKUBE_HOME=${data_home}
export PARALLEL_HOME=${data_home}/parallel
export PGPASSFILE="${config_home}/pg/pgpass"
export PGSERVICEFILE="${config_home}/pg/pg_service.conf"
export PIPX_HOME=${data_home}/pipx
export PSQLRC="${config_home}/pg/psqlrc"
export PSQL_HISTORY=${state_home}/psql_history
export PYENV_ROOT=${data_home}/pyenv
export PYLINTHOME=${cache_home}/pylint
export QMK_HOME=${data_home}/qmk_firmware
export RCRC=${config_home}/rcrc
export SQLITE_HISTORY=${state_home}/sqlite_history
export WGETRC=${config_home}/wgetrc

###############################################################################
# Other configuration
###############################################################################
export JAVA_TOOL_OPTIONS="\
-Djava.util.prefs.userRoot=${config_home} \
-Dawt.useSystemAAFontSettings=on \
-Dswing.aatext=true \
-Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel \
-Duser.country=US \
-Duser.language=en"
export LUTRIS_RUNTIME=0
export MINIKUBE_IN_STYLE=false
export MINIKUBE_WANTUPDATENOTIFICATION=false
export NEOVIDE_FORK=1
export NNN_FCOLORS=0303040200000608010b0500
export NNN_OPTS=cdouAU
export NNN_PLUG=r:renamer
export NNN_TRASH=2
export NO_AT_BRIDGE=1
export PYTHON_BASIC_REPL=1
export QT_QPA_PLATFORMTHEME=qt5ct

###############################################################################
# General configuration
###############################################################################
export EDITOR=nvim
export HISTFILE=${state_home}/bash/history
export PAGER=less
export PATH=${CARGO_HOME}/bin:${GOPATH}/bin:${GEM_HOME}/bin:${HOME}/.local/bin:/opt/go/bin:${PATH}
export VISUAL=nvim


# For greetd's gtkgreet and libadwaita applications
export GTK_THEME=Arc-Dark

unset cache_home \
      config_home \
      data_home \
      state_home

# vim:ft=sh
